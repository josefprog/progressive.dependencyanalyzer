﻿using System.Collections.Generic;
using ServiceStack.Text;
using StackExchange.Redis;

namespace Progressive.DependencyAnalyzer.Controller
{
    public class Data
    {
        internal static string DbName = "Toolworks";
        internal static string TableName = "Dependencies";
        
        // TODO: Parameterize this
        private static string redisServer = "slc-devmsgrdl.stormwind.local:6390";
        private static string redisKeyPrefix = "prog:tools:dependencies:";
        private readonly ConnectionMultiplexer _redis;

        internal Data()
        {
            _redis = ConnectionMultiplexer.Connect(redisServer);
        }

        internal void WriteEntitiesToDb(List<Package> packages, PackageType type)
        {
            foreach (var package in packages)
            {
                // Convert package to JSON string
                var json = JsonSerializer.SerializeToString(package);

                // Write JSON string to redis
                var db = _redis.GetDatabase();
                var value = redisKeyPrefix + type + ":" + package.NameSpace;
                db.StringSet(value, json);
            }
        }

        public enum PackageType
        {
            Project,
            Nuget
        }
    }
}
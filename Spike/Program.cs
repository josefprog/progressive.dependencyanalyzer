﻿using System;

namespace Spike
{
    class Program
    {
        private static readonly Data Data = new Data();

        static void Main(string[] args)
        {
            Console.WriteLine("Starting Spike execution...");

            ValidateParms(args);

            // Inject SQL req's here
            // var connectionString = "Server=tooldev;Database=toolworks;User Id=toolworks;Password = password; MultipleActiveResultSets=False;";

            Console.WriteLine("Command Paramenters validated...");

            var files = new Files(args[0]);

            Console.WriteLine("Files Object Created!");

            var projectPackages = Csproj.ParseCsprojFiles(files.Csproj, files.Repo);
            var nuspecPackages = Nuspec.ParseNuspecFiles(files.Nuspec, files.Repo);

            // Write Project Data
            Data.WriteEntitiesToDb(projectPackages);

            // Write Nuspec Data
            Data.WriteEntitiesToDb(nuspecPackages);

            Console.WriteLine("Enter to Exit Application...");
            Console.ReadLine();
        }


        static void ValidateParms(string[] args)
        {
            // Validation of Parms here
            if (string.IsNullOrEmpty(args[0]))
            {
                Console.WriteLine("You must supply the initial parsing directory as a parameter.  Exiting (1)...");
                Environment.Exit(1);
            }
        }
    }
}

﻿namespace Progressive.DependencyAnalyzer.Controller
{
    public class Dependency
    {
        public string Name { get; set; }
        public bool NuGet { get; set; }
        public bool Project { get; set; }
        public string Version { get; set; }

        public override string ToString()
        {
            return string.Format("{0} [{1}]", Name, Version);
        }
    }
}
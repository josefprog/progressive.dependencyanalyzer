﻿using System;
using System.IO;
using System.Linq;
using LibGit2Sharp;
using LibGit2Sharp.Handlers;

namespace Progressive.DependencyAnalyzer.Controller
{
    public class Git
    {
        private static string _user = "toolworks@progleasing.com";
        private static string _pass = "T00lW0rx";
        private static readonly UsernamePasswordCredentials Cred = new UsernamePasswordCredentials
        {
            Password = _pass,
            Username = _user
        };

        public static string CloneOrFetchRepo(string url)
        {
            var repo = string.Empty;

            if (!DirExists(url))
            {
                // No Dir, clone away
                repo = Clone(url);
            }
            else if (DirExists(url) && IsValidRepo(url))
            {
                // Valid Repo, Fetch all
                var message = FetchAll(url);
                Console.WriteLine($"Git Pull Respose: {message}");
                repo = DirFromName(NameFromUrl(url));
            }
            else
            {
                Console.WriteLine("Error!  Dir must be Empty or a Valid Git Repo...");
            }

            return repo.TrimEnd("\\.git\\".ToCharArray());
        }

        public static string Clone(string url)
        {
            var name = NameFromUrl(url);
            var dir = DirFromName(name);
            var co = new CloneOptions();
            co.CredentialsProvider = (credUrl, credUser, cred) => Cred;

            var repo = Repository.Clone(url, dir, co);

            return repo;
        }

        public static bool IsValidRepo(string url)
        {
            var name = NameFromUrl(url);
            var dir = DirFromName(name);

            return Repository.IsValid(dir);
        }

        public static bool DirExists(string url)
        {
            var name = NameFromUrl(url);
            var dir = DirFromName(name);

            return Directory.Exists(dir);
        }

        public static string FetchAll(string url)
        {
            var name = NameFromUrl(url);
            var dir = DirFromName(name);

            var repo = new Repository(dir);
            var pullOptions = new PullOptions();
            var fetchOptions = new FetchOptions();

            fetchOptions.CredentialsProvider = (credUrl, credUser, cred) => Cred;
            pullOptions.FetchOptions = fetchOptions;

            var results = Commands.Pull(repo,
                new Signature("Toolworks Dependency Scan", "toolworks@progleasing.com", DateTimeOffset.Now),
                pullOptions);

            return results.Status.ToString();
        }

        private static string NameFromUrl(string url)
        {
            var name = url.Split("/").Last().TrimEnd(".git".ToCharArray());

            return name;
        }

        private static string DirFromName(string name)
        {
            var dir = Path.Combine("..\\code", name);

            return dir;
        }
    }
}
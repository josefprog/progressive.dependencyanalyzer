﻿using Progressive.DependencyAnalyzer.Controller;
using ServiceStack;

namespace Progressive.DependencyAnalyzer
{
    public class DependencyService : Service
    {
        [Route("/dependency", "POST")]
        public object Post(DependencyRequest request)
        {
            var rawBody = base.Request.GetRawBody().Replace("\\r\\n", "\r\n");
            var result = Scanner.Process(rawBody);

            return result;
        }

        [Route("/dependency/{NameSpace}", "GET")]
        public object Get(DependencyRequest request)
        {
            // TODO: Get Dependency data for specificied namespace

            // TODO: Add proper return value here
            return null;
        }
    }

    public class DependencyRequest
    {

    }
}
﻿using System;

namespace Spike
{
    public class XmlParse
    {
        internal static string ParseXmlInnerText(string file, string nodeName, int parseStart = 0)
        {
            var prestart = file.IndexOf("<" + nodeName + ">", parseStart, StringComparison.Ordinal);

            if (prestart == -1)
            {
                return string.Empty;
            }

            var start = prestart + nodeName.Length + 2; // an opening bracket + length of nodeName + a closing bracket
            var end = file.IndexOf("</" + nodeName + ">", start, StringComparison.Ordinal);

            return file.Substring(start, end - start);
        }

        internal static string ParseXmlAttribute(string file, string nodeName, string attributeName, int parseStart = 0)
        {
            var prestart = file.IndexOf("<" + nodeName, parseStart, StringComparison.Ordinal);

            if (prestart == -1)
            {
                return string.Empty;
            }

            var attrStart = file.IndexOf(attributeName, prestart, StringComparison.Ordinal);
            var start = attrStart + attributeName.Length + 2; // length of attributeName + "=" + opening quote
            var end = file.IndexOf("\"", start, StringComparison.Ordinal);

            var attrValue = file.Substring(start, end - start);

            return attrValue;
        }
    }
}
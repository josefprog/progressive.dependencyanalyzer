﻿using System;

namespace Progressive.DependencyAnalyzer.Controller
{
    internal class Scanner
    {
        private static readonly Data Data = new Data();

        internal static string Process(string repo)
        {
            // TODO: Need to address Git Security (SSH?  Basic?)

            // Pull down Git repo
            var dir = Git.CloneOrFetchRepo(repo);

            // Process files
            var files = new Files(dir);

            // Process Project and Nuget files
            // TODO: Future Need: tie csproj and package.json together... Not currently used in repos
            var projectPackages = Csproj.ParseCsprojFiles(files.Csproj, files.Repo);
            var nuspecPackages = Nuspec.ParseNuspecFiles(files.Nuspec, files.Repo);

            // Write Project Data
            Data.WriteEntitiesToDb(projectPackages);

            // Write Nuspec Data
            Data.WriteEntitiesToDb(nuspecPackages);

#if(DEBUG)
            Console.WriteLine("Enter to Exit Application...");
            Console.ReadLine();
#endif

            // TODO: Valid return value here
            return "Something here!";
        }


        static void ValidateParms(string[] args)
        {
            // Validation of Parms here
            if (string.IsNullOrEmpty(args[0]))
            {
                Console.WriteLine("You must supply the initial parsing directory as a parameter.  Exiting (1)...");
                Environment.Exit(1);
            }
        }
    }
}

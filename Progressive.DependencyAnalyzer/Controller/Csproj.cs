﻿using System;
using System.Collections.Generic;
using LibGit2Sharp;

namespace Progressive.DependencyAnalyzer.Controller
{
    public class Csproj
    {
        internal static List<Package> ParseCsprojFiles(List<string> projects, Remote repo)
        {
            var results = new List<Package>();

            foreach (var project in projects)
            {
                var file = Files.GetFileContents(project);
                var framework = XmlParse.ParseXmlInnerText(file, "TargetFrameworks");

                // Create Package
                var projectEntity = new Package
                {
                    Name = Files.GetFileName(project),
                    NameSpace = XmlParse.ParseXmlInnerText(file, "RootNamespace"),
                    Assembly = XmlParse.ParseXmlInnerText(file, "AssemblyName"),
                    Version = XmlParse.ParseXmlInnerText(file, "Version"),
                    Framework = framework,
                    LastModified = Files.GetFileLastModified(project)
                };

                // populate entity with project data
                if (repo != null) projectEntity.Repo = repo.Url;
                projectEntity.Dependencies = ParseDependencies(file);

                Console.WriteLine("Found Project: [{0}]", projectEntity);
                Console.WriteLine(projectEntity.Details());

                results.Add(projectEntity);
            }
            
            return results;
        }


        private static List<Dependency> ParseDependencies(string file)
        {
            var results = new List<Dependency>();

            var packageRefs = FindProjectReferences(file, "PackageReference", "Include", true, false);
            var refs = FindProjectReferences(file, "Reference", "Include", true, false);
            var projectRefs = FindProjectReferences(file, "ProjectReference", "Include", false, true);


            results.AddRange(packageRefs);
            results.AddRange(refs);
            results.AddRange(projectRefs);
            return results;
        }

        private static List<Dependency> FindProjectReferences(string file, string nodeName, string attributeName, bool nuGet, bool project)
        {
            // Take Starting point
            var pointer = 0;
            var attrValue = "PlaceHolder";
            var results = new List<Dependency>();
            var framework = GetProjectFramework(file);

            while (attrValue != "")
            {
                attrValue = XmlParse.ParseXmlAttribute(file, nodeName, attributeName, pointer);

                if (attrValue == "") continue; // Continue Loop and Exit
                
                // Add dependancy to result stack
                if (attrValue == "System" || attrValue.StartsWith("System.") || attrValue == "Microsoft.CSharp")
                {
                    // Record results, but clear NuGet and Project Flags for System assemblies
                    results.Add(new Dependency() { Name = attrValue, NuGet = false, Project = false, Version = framework});
                }
                else
                {
                    // Get Dependency Version from Attribure or innerXML
                    var version = FindDependencyVersion(file, pointer);
                    // Record results
                    results.Add(new Dependency() { Name = attrValue, NuGet = nuGet, Project = project, Version = version });
                }

                // Move pointer to just past last entry
                if (file.IndexOf("<" + nodeName + ">", pointer, StringComparison.Ordinal) > -1 &&
                    file.IndexOf("<" + nodeName + " ", pointer, StringComparison.Ordinal) > -1)
                {
                    var innerPointer = file.IndexOf("<" + nodeName + ">", pointer, StringComparison.Ordinal) + nodeName.Length + 1;
                    var inlinePointer = file.IndexOf("<" + nodeName + " ", pointer, StringComparison.Ordinal) + nodeName.Length + 1;
                    pointer = innerPointer < inlinePointer ? innerPointer : inlinePointer;
                } else if (file.IndexOf("<" + nodeName + ">", pointer, StringComparison.Ordinal) > -1)
                {
                    pointer = file.IndexOf("<" + nodeName + ">", pointer, StringComparison.Ordinal) + nodeName.Length + 1;

                }
                else
                {
                    pointer = file.IndexOf("<" + nodeName + " ", pointer, StringComparison.Ordinal) + nodeName.Length + 1;
                }
            }

            return results;
        }

        private static string GetProjectFramework(string file)
        {
            var framework = XmlParse.ParseXmlInnerText(file, "TargetFrameworkVersion");
            if (framework == "")
            {
                framework = XmlParse.ParseXmlInnerText(file, "TargetFrameworks");
                if (framework == "")
                {
                    framework = "??";
                }
            }

            if (framework.StartsWith("v"))
            {
                framework = framework.Substring(1);
            }

            return framework;
        }

        private static string FindDependencyVersion(string file, int pointer)
        {
            // Try inline attibute
            var attrValue = XmlParse.ParseXmlAttribute(file, "PackageReference", "Version", pointer);
            if (attrValue != "") return attrValue;

            // Try inner xml node attribute
            var innerXml = XmlParse.ParseXmlInnerText(file, "PackageReference", pointer);
            attrValue = XmlParse.ParseXmlInnerText(innerXml, "Version");

            if (attrValue == "")
            {
                attrValue = "??";
            }

            return attrValue;
        }
    }
}
﻿using System.ComponentModel;
using System.Net;
using System.Reflection;
using ServiceStack;
using ServiceStack.Text;

namespace Progressive.DependencyAnalyzer
{
    public class AppHost : AppHostBase
    {
        public AppHost() : base("DependecyAnalyzer", typeof(DependencyService).GetTypeInfo().Assembly)
        {
        }

        public override void Configure(Funq.Container container)
        {
            this.CustomErrorHttpHandlers.Remove(HttpStatusCode.Forbidden);
            JsConfig.EmitCamelCaseNames = true;

            Routes.Add<DependencyRequest>("/dependency", ApplyTo.All);
        }
    }
}

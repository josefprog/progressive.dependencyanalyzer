﻿using System;
using System.Collections.Generic;
using LibGit2Sharp;

namespace Spike
{
    public class Nuspec
    {
        public static List<Package> ParseNuspecFiles(List<string> files, Remote repo)
        {
            var results = new List<Package>();

            foreach (var nuspecFile in files)
            {
                var file = Files.GetFileContents(nuspecFile);

                // Create Package
                var nugetEntity = new Package
                {
                    Name = Files.GetFileName(nuspecFile),
                    NameSpace = XmlParse.ParseXmlInnerText(file, "id"),
                    Version = XmlParse.ParseXmlInnerText(file, "version"),
                    LastModified = Files.GetFileLastModified(nuspecFile)
                };

                // populate entity with project data
                if (repo != null) nugetEntity.Repo = repo.Url;
                nugetEntity.Dependencies = ParseDependencies(file);

                Console.WriteLine("Found Nuspec: [{0}]", nugetEntity);
                Console.WriteLine(nugetEntity.Details());

                results.Add(nugetEntity);
            }

            return results;
        }

        private static List<Dependency> ParseDependencies(string file)
        {
            //<dependencies>
            //    <group targetFramework = "net461" >
            //        <dependency id = "ServiceStack.Text" version = "4.0.52" exclude = "Build,Analyzers" />
            //        <dependency id = "log4net" version = "2.0.8" exclude = "Build,Analyzers" />
            //    </group>
            //</dependencies>

            var results = new List<Dependency>();

            var packageRefs = FindNugetReferences(file, "PackageReference", "Include", true);
            var refs = FindNugetReferences(file, "dependency", "id", true);


            results.AddRange(packageRefs);
            results.AddRange(refs);
            return results;
        }

        private static List<Dependency> FindNugetReferences(string file, string nodeName, string attributeName, bool nuGet)
        {
            // Take Starting point
            var pointer = 0;
            var attrValue = "PlaceHolder";
            var results = new List<Dependency>();

            while (attrValue != "")
            {
                attrValue = XmlParse.ParseXmlAttribute(file, nodeName, attributeName, pointer);
                var versionValue = XmlParse.ParseXmlAttribute(file, nodeName, "version", pointer);

                if (attrValue == "") continue; // Continue Loop and Exit


                // Add dependancy to result stack
                if (attrValue == "System" || attrValue.Contains("System.") || attrValue == "Microsoft.CSharp")
                {
                    // Record results, but clear NuGet and Project Flags for System assemblies
                    results.Add(new Dependency() { Name = attrValue, Version = versionValue, NuGet = false, Project = false });
                }
                else
                {
                    // Record results
                    results.Add(new Dependency() { Name = attrValue, Version = versionValue, NuGet = nuGet, Project = false });
                }
                // Move pointer to just past last entry
                pointer = file.IndexOf(nodeName, pointer, StringComparison.Ordinal) + 17;
            }

            return results;
        }
    }
}
﻿using System;
using System.IO;
using System.Linq;
using LibGit2Sharp;

namespace Spike
{
    public class Git
    {
        public static string Clone(string url)
        {
            var name = NameFromUrl(url);
            var dir = DirFromName(name);
            var repo = Repository.Clone(url, dir);

            return repo;
        }

        public static bool IsValidRepo(string url)
        {
            var name = NameFromUrl(url);
            var dir = DirFromName(name);

            return Repository.IsValid(dir);
        }

        public static bool DirExists(string url)
        {
            var name = NameFromUrl(url);
            var dir = DirFromName(name);

            return Directory.Exists(dir);
        }

        public static string FetchAll(string url)
        {
            var name = NameFromUrl(url);
            var dir = DirFromName(name);

            var repo = new Repository(dir);
            var results = Commands.Pull(repo,
                new Signature("Toolworks Dependency Scan", "toolworks@progleasing.com", DateTimeOffset.Now),
                new PullOptions());

            return results.Status.ToString();
        }

        private static string NameFromUrl(string url)
        {
            var name = url.Split("/").Last().TrimEnd(".git".ToCharArray());

            return name;
        }

        private static string DirFromName(string name)
        {
            var dir = Path.Combine(".\\code", name);

            return dir;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LibGit2Sharp;

namespace Spike
{
    class Files
    {
        internal List<string> Csproj { get; set; }
        internal List<string> PackagesConfig { get; set; }
        internal List<string> Nuspec { get; set; }

        internal Remote Repo { get; set; }

        internal Files(string rootPath)
        {
            // Initialize Properties
            Csproj = new List<string>();
            PackagesConfig = new List<string>();
            Nuspec = new List<string>();

            // Validate Path
            if (!Directory.Exists(rootPath))
            {
                throw new Exception("Invalid Directory path specified...");
            }

            var csproj = Directory.GetFiles(rootPath, "*.csproj", SearchOption.AllDirectories);
            Console.WriteLine("Found [{0}] csproj files...", csproj.Length);
            Csproj.AddRange(csproj);

            var package = Directory.GetFiles(rootPath, "packages.config", SearchOption.AllDirectories);
            Console.WriteLine("Found [{0}] packages.config files...", package.Length);
            PackagesConfig.AddRange(package);

            var nuspec = Directory.GetFiles(rootPath, "*.nuspec", SearchOption.AllDirectories);
            Console.WriteLine("Found [{0}] nuspec files...", nuspec.Length);
            Nuspec.AddRange(nuspec);

            if (Repository.IsValid(rootPath))
            {
                var repo = new Repository(rootPath);
                if (repo.Network.Remotes.Any() && repo.Network.Remotes["origin"] != null)
                {
                    Repo = repo.Network.Remotes["origin"];
                }
            }
        }

        internal static string GetFileName(string filePath)
        {
            var filename = Path.GetFileNameWithoutExtension(filePath);

            return filename;
        }

        internal static DateTime GetFileLastModified(string filePath)
        {
            var lastMod = File.GetLastWriteTime(filePath);

            return lastMod;
        }

        internal static string GetFileContents(string filePath)
        {
            var file = File.ReadAllText(filePath);

            return file;
        }

    }
}

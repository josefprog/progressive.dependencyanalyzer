﻿using System;
using System.Collections.Generic;
using ServiceStack.Text;
using StackExchange.Redis;

namespace Spike
{
    public class Data
    {
        internal static string DbName = "Toolworks";
        internal static string TableName = "Dependencies";
        
        private static string redisServer = "slc-devmsgrdl.stormwind.local:6390";
        private static string redisKeyPrefix = "prog:tools:dependencies:";
        private readonly ConnectionMultiplexer _redis;

        internal Data()
        {
            _redis = ConnectionMultiplexer.Connect(redisServer);
        }

        internal string TestRead()
        {
            var db = _redis.GetDatabase();
            var value = db.StringGet(redisKeyPrefix + ":test");

            return value;
        }
        

        internal void WriteEntitiesToDb(List<Package> packages)
        {
            foreach (var package in packages)
            {
                // Convert package to JSON string
                var json = JsonSerializer.SerializeToString(package);

                // Write JSON string to redis
                var db = _redis.GetDatabase();
                var value = redisKeyPrefix + package.NameSpace;
                db.StringSet(value, json);
            }
        }

    }
}
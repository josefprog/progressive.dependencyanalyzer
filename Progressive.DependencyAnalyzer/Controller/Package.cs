﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Progressive.DependencyAnalyzer.Controller
{
    public class Package
    {
        public string Name { get; set; }
        public string Assembly { get; set; }
        public string Framework { get; set; }
        public string NameSpace { get; set; }
        public string Version { get; set; }
        public string Repo { get; set; }
        public DateTime LastModified { get; set; }
        public List<Dependency> Dependencies { get; set; }

        public Package()
        {
            Dependencies = new List<Dependency>();
        }

        public override string ToString()
        {
            return string.Format("{0} [{1} dependencies]", Name, Dependencies.Count);
        }

        public string Details()
        {
            var result = new StringBuilder();
            result.AppendFormat("\tName: {0}\r\n\tAssembly: {1}\r\n\tNamespace: {2}\r\n\tVersion: {3}\r\n\tRepo: {4}\r\n\tLast Modified: {5}\r\n",
                Name,
                Assembly,
                NameSpace,
                Version,
                Repo,
                LastModified);

            foreach (var dependency in Dependencies)
            {
                // Add question marks for unknown versions
                var version = dependency.Version ?? "??";

                result.AppendFormat("\t\tDependency: {0} v.{1}", dependency.Name, version);

                if (dependency.NuGet)
                {
                    result.AppendFormat(" (NuGet) ");
                }

                if (dependency.Project)
                {
                    result.AppendFormat(" (csproj) ");
                }

                result.Append("\r\n");
            }

            return result.ToString();
        }
    }
}